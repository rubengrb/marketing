```
This is a draft checklist for things that should be completed with each release:

 - [] Noteworthy features are agreed upon and documented in the release issue (all release issue should use the "release" label)
 - [] New features are scheduled for or already demo'ed at GitLab University
 - [] New features are fully documented with screenshots (align with technical writers)
 - [] New features are shown on /features
 - [] New features appear in the compare view on /features
 - [] Feature highlights are planned with the new features
 - [] /direction is updated (should be automated soon enough)
 - [] Evaluate the opportunity to update existing screens or copy on pages that will be updated for the release
 - [] Document changes and new features
```
