Pre-webinar 
- [ ] Create webcast copy (Erica) 
- [ ] Create the webcast registration page (Mitchell)
- [ ] Send calendar invitation (with 15 minute buffer) to presenters with presenter link (Mitchell)
- [ ] Send webcast dry run calendar invitation to presenters (Mitchell)
- [ ] Create ads promoting webcast (Mitchell)
- [ ] Promote webcast in newsletter (Erica)
- [ ] Schedule webcast promo tweets T - 3 weeks, T - 2 weeks, T - 3 days, and T - 1 day, and T (Erica)
- [ ] Add webinar to banner on about.gitlab.com (Erica)
- [ ] Create and share webcast deck for feedback (Erica/Presenter)

Days before webinar
- [ ] Host webcast dry run and ensure everyone is comfortable with platform (Mitchell and Erica)
- [ ] Load final slides, polls, and resources into webcast platform (Mitchell)

During webinar
- [ ] Erica, and Mitchell to manage presentation
- [ ] Record the webinar

Post webinar
- [ ] Edit the recording (Erica)
- [ ] Post on YouTube, Speaker Deck, and blog (Erica) 
- [ ] Promote blog post on Twitter (Erica) 
- [ ] Send webinar follow up emails (Mitchell)