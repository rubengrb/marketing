### Overview

*[ Include a brief overview / description of the project here ]*

### Goal

*What is the overall goal of this project and how will it be accomplished?*

### Target audience

*To gain an understanding of the users, who is the target audience?*

### Design requirements

- Example requirements list
- Print specs
- Templates / attachments
- File formats
- Vendor information
- Links

### Design deliverables

- [ ] Design deliverable 1 w/ due date if applicable (i.e ~ 3"x3" die-cut sticker)
- [ ] Design deliverable 2 w/ due date if applicable
- [ ] Design deliverable 3 w/ due date if applicable

### Content/copy deliverables

- [ ] Content deliverable 1 w/ due date if applicable
- [ ] Content deliverable 2 w/ due date if applicable
- [ ] Content deliverable 3 w/ due date if applicable
